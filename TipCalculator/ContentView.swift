//
//  ContentView.swift
//  TipCalculator
//
//  Created by Giuseppe Ferrara on 26/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import SwiftUI
import Combine

struct ContentView: View {
    @ObservedObject var tipViewModel = TipViewModel()
    
    var body: some View {
        VStack {
            TextField("Enter an amount", text: $tipViewModel.amount)
            Picker(selection: $tipViewModel.tipIndex, label: Text("Select the tip percentage")) {
                ForEach(0..<tipViewModel.tipChoices.count-1){
                    index in
                    Text("\(self.tipViewModel.tipChoices[index])").tag(index)
                }
            }.pickerStyle(SegmentedPickerStyle())
                .onTapGesture {
                    self.tipViewModel.calculateTip()
            }
            Text("the tip is : \(tipViewModel.tip)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
