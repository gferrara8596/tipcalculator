//
//  TipViewModel.swift
//  TipCalculator
//
//  Created by Giuseppe Ferrara on 26/01/2020.
//  Copyright © 2020 Giuseppe Ferrara. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class TipViewModel: ObservableObject {
    
    var amount: String = ""
    @Published var tip: String = "0"
    var tipIndex = 0
    let tipChoices = [2,5,10,15,20,25,30]
    
    func calculateTip(){
        print("calculating tip...")
        guard let safeAmount = Double(amount) else {return }
        let tipCalculated = (safeAmount * Double(tipChoices[tipIndex]))/100.0
        tip = String(tipCalculated.truncate(places: 2))
    }
    
}

extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
